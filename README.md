# Arvados Cluster for SURF Research Cloud

## Description

This is a set of Ansible Playbooks that set up an [Arvados](https://doc.arvados.org/v2.4/) cluster.

The targeted infrastructure is [SURF Research Cloud](https://www.surf.nl/en/surf-research-cloud-collaboration-portal-for-research).

In the course of the project's progress, more details on the setup of a cluster will be given here.

## Installation

Research Cloud is wrapping Ansible Playbooks into "plugins" which are run by the Research Cloud deployer to create a particular "turn-key" virtual resource.
To use this code in the intended way, you'd need access to SURF Research Cloud and just start the "Arvados-Cluster" application which will in turn trigger the execution of the Ansible playbooks here.

[Research Cloud user docs](https://servicedesk.surf.nl/wiki/x/HIKV)

In the Ansible-setup we follow the steps as given in
[Arvados cluster manual installation](https://doc.arvados.org/v2.4/install/install-manual-prerequisites.html)

Still, the Ansible installation uses the Nginx plugin of Research Cloud, not the suggested installation method.

### Create the cluster hosts on SURF Research Cloud

- Create a collaboration (CO) with the name that will be your Arvados cluster ID (5 characters)
- Create a CO-secret. Key: "arvados_api_token_secret", secret value: a random uuid without hyphens. Only \[a-z\], \[A-z\] and \[0-9\] are allowed.
- Create a CO-secret. Key: "s3_access_key_secret", secret value: &lt;your S3 secret access key&gt;
- Create a CO-secret. Key: "s3_access_key", secret value: &lt;your S3 account access&gt;
- Create a CO-secret. Key: "samba_password", secret value: a random uuid
- In the Research Cloud portal create a private network.
- In the Research Cloud portal create a external storage volume. Dimension it such that it can accommodate the cluster's postgres-database.
- Create a workspace with the `Arvados-core-host` application.
  During creation, attach it to the private network and the external storage.
  This workspace's hostname must be `core`
- Confirm and submit the core-host's creation.
- Every few minutes, download the creation-logfile of the core-host. As soon as the "PRIVATE IP OF SAMBA-SERVER" is given in the logfile it is time to start the keep servers.
- While the core server is still creating, create two workspaces using the `Arvados-keep-storage` application, and one workspace using the `Arvados keepproxy` application.
  The hostnames must, respectively, be
  - `keep0`
  - `keep1`
  - `keep`
  Make sure that the workspaces are attached to the created private network.
- The core server will only finish creating when all other hosts have submitted their connection data to the shared Samba-drive.
- 'core' and 'keep' workspaces should enter the "running state", at some point.
- when core and keeps are running, you can start the workbench host. Make sure that it is on the same private network as the other hosts, in order to retrieve the arvados config file.

### Setting up the Snellius-users home directory for Arvados

#### Create an arvados directory on Snellius

Log in to Snellius (to an interactive Snellius-node, that is).

Note on which actual interactive node of Snelius you have logged in. The terminal prompt will tell you.

The crunch-dispatch-slurm service will run on this node. Refer to this node explicitly for the next steps. Interactive nodes on Snellius have a host naming pattern of "int{number}-pub.snellius.surf.nl". Let's **assume** that we have logged in to **"int4-pub"**, now. Your number might be different.

Create a temporary installation directory in your Snellius-home.
```bash
mkdir -p ~/arvados/tmp
```

#### Copy installation files to Snellius.
Log in to the core host.
From the core host, copy the files in the "/etc/arvados/to-snellius" directory to the new ~/arvados/tmp directory on Snellius:
```bash
scp /etc/arvados/to-snellius/* carstesc@int4-pub.snellius.surf.nl:~/arvados/tmp
```

#### Establish port forwarding

By local port forwarding on the core host make sure that any traffic on the core host to "http://localhost:9007" gets forwarded to Snellius' 9007 port. (Log in to Snellius while this command executes)
```bash
ssh -NfL 9007:int4-pub.snellius.surf.nl:9007 carstesc@snellius.surf.nl
```

#### On Snellius, make Postgres db reachable
Edit the file
~/arvados/tmp/config.yml

Change the value "localhost" of Clusters.PostgreSQL.Connection.host to the public ip-address of the arvados-cluster's *core* host.


#### On Snelluis, run the snellius-setup script

```bash
chmod 777 ~/arvados/tmp/snellius-setup.sh
~/arvados/tmp/snellius-setup.sh
```

This will install all remaining files and start the slurm dispatcher service.

#### Port forward to core host's postgres-port

- Create a private-public key pair on Snellius
- Add the public key to your SRAM profile
- Establish a local port forward from Snellius to the core host 5432 port
```
ssh -NfL 19007:<core host ip>:5432 <core host user>@<core host ip>
```
- On Snellius, edit the local version of the arvados config.yml file
  - In the PostgreSQL section set
    - **host** to "localhost"
    - **port** to "19007" (the quotes around 19007 do matter)

- Restart the dispatcher service
```
systmctl --user restart crunch-dispatch-slurm
```

#### Check the dispatcher service

```bash
systemctl --user status crunch-dispatch-slurm
journalctl --user -xe
```

## Project status

This project is under initial construction.
