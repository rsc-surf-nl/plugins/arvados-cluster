#!/bin/bash

# assuming that config.yml, the unit file and this are in ~/arvados/tmp.

mv ~/arvados/tmp/config.yml ~/arvados/
# Create environment file with config-path setting.
echo ARVADOS_CONFIG=/home/$USER/arvados/config.yml >> ~/arvados/environment

# crunch-dispatch-slurm service unit file goes here
mkdir -p ~/.config/systemd/user/
mv ~/arvados/tmp/crunch-dispatch-slurm.service ~/.config/systemd/user/
sed -i s/_snellius_user_/$USER/g ~/.config/systemd/user/crunch-dispatch-slurm.service


wget http://rpm.arvados.org/CentOS/7/os/x86_64/crunch-dispatch-slurm-2.5.0-1.x86_64.rpm -P ~/arvados/tmp/
wget http://rpm.arvados.org/CentOS/7/os/x86_64/crunch-run-2.5.0-1.x86_64.rpm -P ~/arvados/tmp/

# Extract dispatcher files from rpm packages
rpm2cpio ~/arvados/tmp/crunch-dispatch-slurm-2.5.0-1.x86_64.rpm | cpio -idmv
# creates: 
# ~/usr/bin/crunch-dispatch-slurm
# and
# ~/lib/systemd/system/crunch-dispatch-slurm.service
# (This extracted systemd-unit file is just for occasional reference, though.
# We use the prepared unit file from the ansible files directory.)

# Extract crunch-run binaries
rpm2cpio ~/arvados/tmp/crunch-run-2.5.0-1.x86_64.rpm | cpio -idmv
# creates:
# ~/usr/bin/crunch-run

mkdir ~/arvados/bin/

# move binaries to arvados/bin dir
mv ~/usr/bin/crunch-dispatch-slurm ~/arvados/bin/
mv ~/usr/bin/crunch-run ~/arvados/bin/
# add bin dir to path
export PATH=$PATH:~/arvados/bin

# clean up temporary directories
rm -r ~/usr
rm -r ~/lib

# We would have needed:
# sudo yum -y install libcurl-devel fuse-devel python3-devel
# But Martijn Kruiten put fuse-devel into the standard-package
# so we only have to use pip3, no "sudo yum..."
pip3 install --user arvados_fuse

loginctl enable-linger $USER
systemctl --user daemon-reload
systemctl --user enable crunch-dispatch-slurm
systemctl --user start crunch-dispatch-slurm



